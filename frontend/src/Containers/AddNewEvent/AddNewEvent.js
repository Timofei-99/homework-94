import React, { useState } from "react";
import { Button, Grid, Typography } from "@material-ui/core";
import FormElement from "../../Components/UI/Form/FormElement";
import { useDispatch } from "react-redux";
import { createEventRequest } from "../../store/actions/eventsActions";

const AddNewEvent = () => {
    const dispatch = useDispatch();
    const [event, setEvent] = useState({
        title: "",
        duration: "",
        date: "",
    });

    const inputChangeHandler = (e) => {
        const { name, value } = e.target;

        setEvent((prev) => ({ ...prev, [name]: value }));
    };

    const submitFormHandler = (e) => {
        e.preventDefault();
        dispatch(createEventRequest({ ...event }));
    };

    return (
        <Grid container spacing={1} direction="column" component="form" onSubmit={submitFormHandler}>
            <Typography variant="h4">Add New Events</Typography>

            <Grid item>
                <FormElement
                    type="text"
                    value={event.title}
                    onChange={inputChangeHandler}
                    label="Title"
                    name="title"
                />
            </Grid>
            <Grid item>
                <FormElement
                    type="text"
                    value={event.duration}
                    onChange={inputChangeHandler}
                    label="Duration"
                    name="duration"
                />
            </Grid>
            <Grid item>
                <FormElement type="text" value={event.date} onChange={inputChangeHandler} label="Date" name="date" />
            </Grid>
            <Grid item>
                <Button type="submit" variant="outlined" onClick={submitFormHandler}>
                    submit
                </Button>
            </Grid>
        </Grid>
    );
};

export default AddNewEvent;

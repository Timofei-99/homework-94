import usersSlice from "../slices/usersSlice";

export const {
    registerRequest,
    registerSuccess,
    registerFailure,
    loginRequest,
    loginSuccess,
    loginFailure,
    logoutSuccess,
    facebookLoginRequest,
    logoutRequest,
    addNewFriendFailure,
    addNewFriendSuccess,
    addNewFriendRequest,
    deleteFriendFailure,
    deleteFriendSuccess,
    deleteFriendRequest,

    fetchFriendFailure,
    fetchFriendSuccess,
    fetchFriendRequest,
} = usersSlice.actions;

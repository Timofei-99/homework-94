import {put, takeEvery} from "redux-saga/effects";
import axiosCalendar from "../../axiosCalendar";
import {
    createEventFailure,
    createEventRequest,
    createEventSuccess,
    deleteEventFailure,
    deleteEventRequest,
    deleteEventSuccess,
    fetchEventsFailure,
    fetchEventsRequest,
    fetchEventsSuccess,
} from "../actions/eventsActions";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";

export function* fetchEvents() {
    try {
        const response = yield axiosCalendar.get("/events");
        yield put(fetchEventsSuccess(response.data));
    } catch (e) {
        yield put(fetchEventsFailure(e));
        toast.error("Failed to fetch events");
    }
}

export function* createEvents({payload: eventsData}) {
    try {
        yield axiosCalendar.post("/events", eventsData);
        yield put(createEventSuccess());
        yield put(historyPush("/"));
        toast.success("You Create New Event");
    } catch (e) {
        yield put(createEventFailure(e));
        toast.error("Failed to create events");
    }
}

export function* deleteEvent({payload: id}) {
    try {
        yield axiosCalendar.delete("/events/" + id);
        yield put(deleteEventSuccess());
        toast.success("You Deleted  Event");
        yield fetchEvents();
    } catch (e) {
        yield put(deleteEventFailure(e));
        toast.error("Failed to deleted events");
    }
}

const eventsSagas = [
    takeEvery(fetchEventsRequest, fetchEvents),
    takeEvery(createEventRequest, createEvents),
    takeEvery(deleteEventRequest, deleteEvent),
];

export default eventsSagas;

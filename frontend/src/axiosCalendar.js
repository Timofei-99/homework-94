import axios from "axios";
import {apiUrl} from "./config";

const axiosCalendar = axios.create({
    baseURL: apiUrl
});

export default axiosCalendar;
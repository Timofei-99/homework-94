const express = require('express');
const auth = require('../middleware/auth');
const Event = require('../models/Event');
const router = express.Router();


router.get('/', auth, async (req, res) => {
    const user = req.user;

    try {
        if (user.friends.length > 0) {
            const eventFromPromise = await Promise.all(
                user.friends.map((friend) => {
                    return Event.find({author: friend}).populate('author', 'displayName');
                })
            );
            const a = eventFromPromise[0];
            const events = await Event.find({author: user._id}).populate('author', 'displayName');

            const allEvents = a.concat(events);

            const b = await Promise.all(allEvents);
            return res.send(b);
        }
        const myEvent = await Event.find({author: user._id}).populate('author', 'displayName');
        return res.send(myEvent);
    } catch (e) {
        res.status(500).send(e);
    }
})

router.post('/', auth, async (req, res) => {
    try {
        const event = new Event({
            title: req.body.title,
            date: req.body.date,
            duration: req.body.duration,
            author: req.user._id
        });

        await event.save()
        return res.send(event);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.delete("/:id", auth, async (req, res) => {
    try {
        const event = await Event.findOne({_id: req.params.id});
        if (event.author.toString() === req.user.id.toString()) {
            await Event.deleteOne({_id: req.params.id});
            return res.send("Deleted");
        }

        return res.send("You cannot delete Not your Event");
    } catch (e) {
        return res.sendStatus(500);
    }
});

module.exports = router;
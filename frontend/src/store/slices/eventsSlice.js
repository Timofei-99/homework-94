import {createSlice} from "@reduxjs/toolkit";

const name = "events";

const eventsSlice = createSlice({
    name,
    initialState: {
        eventsLoading: false,
        eventsError: null,
        events: [],
    },
    reducers: {
        fetchEventsRequest: (state, action) => {
            state.eventsLoading = true;
        },
        fetchEventsSuccess: (state, {payload: events}) => {
            state.eventsLoading = false;
            state.events = events;
        },
        fetchEventsFailure: (state, {payload: error}) => {
            state.eventsLoading = false;
            state.eventsError = error;
        },
        createEventRequest: (state, action) => {
            state.eventsLoading = true;
        },
        createEventSuccess: (state, action) => {
            state.eventsLoading = false;
        },
        createEventFailure: (state, {payload: error}) => {
            state.eventsLoading = false;
            state.eventsError = error;
        },
        deleteEventRequest: (state, action) => {
            state.eventsLoading = true;
        },
        deleteEventSuccess: (state, action) => {
            state.eventsLoading = false;
        },
        deleteEventFailure: (state, {payload: error}) => {
            state.eventsLoading = false;
            state.eventsError = error;
        },
    },
});

export default eventsSlice;

import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import {initialState} from "./slices/usersSlice";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./rootSaga";
import {configureStore} from "@reduxjs/toolkit";
import rootReducer from "./rootReducer";
import axiosCalendar from "../axiosCalendar";

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: rootReducer,
    middleware: [sagaMiddleware],
    devTools: true,
    preloadedState: loadFromLocalStorage(),
});

sagaMiddleware.run(rootSaga);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            ...initialState,
            user: store.getState().users.user,
        },
    });
});

axiosCalendar.interceptors.request.use((config) => {
    try {
        config.headers["Authorization"] = store.getState().users.user.token;
    } catch (e) {
        // do nothing, no token exists
    }

    return config;
});

axiosCalendar.interceptors.response.use(
    (res) => res,
    (e) => {
        if (!e.response) {
            e.response = {data: {global: "No internet"}};
        }

        throw e;
    }
);

export default store;

import {combineReducers} from "redux";
import eventsSlice from "./slices/eventsSlice";
import usersSlice from "./slices/usersSlice";

const rootReducer = combineReducers({
    users: usersSlice.reducer,
    events: eventsSlice.reducer,
});

export default rootReducer;

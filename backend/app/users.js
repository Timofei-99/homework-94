const express = require('express');
const User = require('../models/User');
const config = require('../config');
const axios = require('axios');
const {nanoid} = require('nanoid');
const router = express.Router();
const auth = require('../middleware/auth');


router.get("/friends", auth, async (req, res) => {
    const user = req.user;
    try {
        if (user.friends.length > 0) {
            const friends = await Promise.all(
                user.friends.map((friend) => {
                    console.log(friend);
                    return User.find({_id: friend});
                })
            );

            console.log(friends);

            return res.send({friends});
        }
    } catch (e) {
        return res.sendStatus(500);
    }
});

router.post("/", async (req, res) => {
    const userData = {
        email: req.body.email,
        password: req.body.password,
        displayName: req.body.displayName,
    };

    try {
        const user = new User(userData);

        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({email: req.body.email});

    if (!user) {
        return res.status(401).send({message: 'Credentials are wrong!'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({message: 'Credentials are worng!'});
    }

    user.generateToken();
    await user.save({validateBeforeSave: false});

    res.send({message: 'Username and password correct!', user});
});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + "|" + config.facebook.appSecret;

    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);
        console.log('RESPONSE: ', response)

        if (response.data.data.error) {
            return res.status(401).send({message: "Facebook token incorected"});
        }

        if (response.data.data.user_id !== req.body.id) {
            return res.status(401).send({global: "User ID incorected"});
        }

        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            user = new User({
                email: req.body.email,
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
            })
        }
        user.generateToken();
        user.save({validateBeforeSave: false});

        return res.send({message: "Success", user});
    } catch (e) {
        return res.status(401).send({global: "Facebook token incorected"});
    }
});


router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Success'};

    if (!token) return res.send(success);

    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();

    await user.save({validateBeforeSave: false});
    return res.send(success);
});

router.put("/addFriends", auth, async (req, res) => {
    try {
        const user = req.user;
        console.log(req.body);

        const friend = await User.findOne({_id: req.body.friend});

        if (!friend) {
            return res.send({message: "No such user"});
        }
        user.friends.push(friend._id);

        await user.save();

        return res.send(user);
    } catch (e) {
        return res.sendStatus(400);
    }
});

router.put("/deleteFriends", auth, async (req, res) => {
    try {
        const user = req.user;
        const friends = [...user.friends];

        const toDelete = friends.find((friend) => {
            if (friend.toString() === req.body.friend.toString()) {
                return friend;
            }
        });
        if (!toDelete) {
            return res.send({message: "No such user"});
        }

        friends.splice(toDelete, 1);

        user.friends = friends;

        await user.save();
        return res.send(user);
    } catch (e) {
        return res.sendStatus(400);
    }
});


module.exports = router;